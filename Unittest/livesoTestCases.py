import socket
import unittest
import threading

class ListeningClass(threading.Thread):
    def __init__(self, socket, keep_alive):
        threading.Thread.__init__(self)
        self.socket = socket
        self.response = None
        self.lookingFor = None
        self.keep_alive = keep_alive

    def run(self):
        full_data = ''
        while self.keep_alive:
            try:
                data = self.socket.recv(4096)
                full_data += data
            except socket.timeout as e:
                if full_data:
                    pass
                continue
            except socket.error as e:
                break

class LiveSoTests(unittest.TestCase):

    def setUp(self):
        # TODO nawiązanie połączenia (z timeout) + utworzenie nasłuchującego wątku
        # + połączenie się + uzyskanie sessionId
        pass

    def test_liveSo(self):
        pass

    def tearDown(self):
        # TODO - zamknięcie połączenie z LiveSo
        # + zamknięcie wątku nasłuchującego
        pass

if __name__ == '__main__':
    unittest.main()