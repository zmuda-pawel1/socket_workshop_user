import threading
import socket
from Tkinter import *
import tkMessageBox

class LoggingClass(threading.Thread):
    def __init__(self, gui, socket):
        threading.Thread.__init__(self)
        self.socket = socket
        self.gui = gui

    def run(self):
        full_data = ''
        while self.gui.keep_alive:
            try:
                data = self.socket.recv(4096)
                full_data += data
            except socket.timeout as e:
                if full_data:
                    self.gui.logMessageBox.insert(1.0, 'RECEIVED:' + full_data + '\n')
                    full_data = ''
                continue
            except socket.error as e:
                self.gui.logMessageBox.insert(1.0, 'INFO: Connection closed\n')
                self.gui.loggingThread.keep_alive = False
                self.gui.connectionState.set('Not Connected')
                self.gui.connectionLabel.config(bg='red')
                self.gui.connectButton.config(text='Connect')
                self.gui.connectButton.config(command=self.gui.connect)
                self.gui.sendButton.config(state=DISABLED)
                break

class TkinterClass(Tk):
    def __init__(self):
        Tk.__init__(self)
        self.pad = 5
        self.initializeInterface()
        self.keep_alive = True

    def initializeInterface(self):
        self.host = StringVar()
        self.hostLabel = Entry(self, textvariable=self.host)
        self.host.set('Host')
        self.hostLabel.grid(column=0, row=0, padx=self.pad, pady=self.pad)

        self.port = StringVar()
        self.portLabel = Entry(self, textvariable=self.port)
        self.port.set('Port')
        self.portLabel.grid(column=1, row=0, padx=self.pad, pady=self.pad)

        self.connectButton = Button(self, text='Connect', command=self.connect)
        self.connectButton.grid(column=2, row=0, padx=self.pad, pady=self.pad)

        self.connectionState = StringVar()
        self.connectionLabel = Label(self, textvariable=self.connectionState, bg='red')
        self.connectionState.set('Not Connected')
        self.connectionLabel.grid(column=3, row=0, padx=self.pad, pady=self.pad)

        self.sendMessageBox = Text(self, height=10, width=50)
        self.sendMessageBox.grid(column=0, row=1, columnspan=3, padx=self.pad, pady=self.pad)

        self.sendButton = Button(self, text='Send Message', command=self.sendMessage, state=DISABLED)
        self.sendButton.grid(column=3, row=1, padx=self.pad, pady=self.pad)

        self.logMessageBox = Text(self, height=20, width=50)
        self.logMessageBox.grid(column=0, row=2, columnspan=4, padx=self.pad, pady=self.pad, sticky='WE')

    def connect(self):
        host = self.host.get()
        port = self.port.get()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.socket.connect((host, int(port)))
            self.socket.settimeout(0.1)
            self.loggingThread = LoggingClass(self, self.socket)
            self.loggingThread.start()
            self.connectionState.set('Connected')
            self.connectionLabel.config(bg='green')
            self.connectButton.config(text='Disconnect')
            self.connectButton.config(command=self.disconnect)
            self.sendButton.config(state=NORMAL)
        except Exception as e:
            self.logMessageBox.insert(1.0, 'ERROR: ' + str(e) + '\n')

    def disconnect(self):
        self.socket.close()

    def sendMessage(self):
        message = self.sendMessageBox.get(1.0, END)
        self.socket.send(message)


tkinter_client = TkinterClass()
tkinter_client.title('Socket Client GUI')
def on_closing():
    if tkMessageBox.askokcancel("Quit", "Do you want to quit?"):
        tkinter_client.keep_alive = False
        tkinter_client.socket.close()
        tkinter_client.destroy()
tkinter_client.protocol("WM_DELETE_WINDOW", on_closing)
tkinter_client.mainloop()